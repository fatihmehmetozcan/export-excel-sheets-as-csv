'Excel module to export sheets as Csv file format

'Fatih Mehmet Özcan
'2019.01
'Use at your own risk

Attribute VB_Name = "Module1csv"
Option Explicit
Private Const address = "excel files directory location"
Private Const outputDirectory = "output directory location" & "\"

Dim counterMain, counterSub, i, tempTrigger, counterTotal As Integer
Dim tempTextSub, tempTextMain, tempCSV, tempWkName, tempShName, fullPath, tempTextTotal As String

Sub main999()

Application.ScreenUpdating = False
Application.DisplayAlerts = False

tempTextMain = Empty
tempTextSub = Empty
counterSub = 0
counterMain = 1
tempCSV = Empty
tempTextTotal = Empty
tempTrigger = 0
counterTotal = 0
loopFolders address

Application.ScreenUpdating = True
Application.DisplayAlerts = True

End Sub

Private Function doStuff(ByVal excelFileString As Variant)
    
    Dim sh As Worksheet
    Dim wk As Workbook
    Dim tempCell As Variant
    On Error GoTo HataDevam
    Set wk = Application.Workbooks.Open(excelFileString, UpdateLinks:=False, ReadOnly:=True)
    counterMain = counterMain + 1
    fullPath = Empty
    tempWkName = fixString(wk.Name)
    'Debug.Print tempWkName
    For Each sh In wk.Worksheets
    
counterTotal = counterTotal + 1
tempTrigger = 1

        If counterTotal < 10 Then
        tempTextTotal = "000" & counterTotal
        ElseIf counterTotal > 9 And counterTotal < 100 Then
        tempTextTotal = "00" & counterTotal
        ElseIf counterTotal > 99 And counterTotal < 1000 Then
        tempTextTotal = "0" & counterTotal
        Else
        tempTextTotal = counterTotal
        End If
        
        fullPath = Empty
        tempShName = fixString(sh.Name)
                counterSub = counterSub + 1
                If counterSub < 10 Then
                tempTextSub = "0" & counterSub
                Else
                tempTextSub = counterSub
                End If
                sh.Select
                tempCSV = outputDirectory & tempTextTotal & "+" & tempTextMain & "+" & _
                tempTextSub & "+" & tempWkName & _
                tempShName & ".csv"
            'Debug.Print tempCSV
            wk.SaveAs fileName:=tempCSV, FileFormat:=xlCSV, CreateBackup:=False

    Next sh

wk.Close False
Set wk = Nothing
tempTextSub = Empty
tempTextMain = Empty
tempCSV = Empty
counterSub = 0

Exit Function

HataDevam:
counterSub = 0
Set wk = Nothing
tempTextSub = Empty
tempTextMain = Empty
tempCSV = Empty

If tempTrigger = 0 Then
counterTotal = counterTotal + 1
        If counterTotal < 10 Then
        tempTextTotal = "000" & counterTotal
        ElseIf counterTotal > 9 And counterTotal < 100 Then
        tempTextTotal = "00" & counterTotal
        ElseIf counterTotal > 99 Then
        tempTextTotal = "0" & counterTotal
        Else
        tempTextTotal = counterTotal
        End If
        tempTrigger = 0
End If

End Function

Private Function fixString(ByVal fullPath As String)
 
For i = 1 To Len(fullPath)
If Mid(fullPath, i, 1) = "\" Or Mid(fullPath, i, 1) = Chr(34) Then
Mid(fullPath, i, 1) = "+"
End If
Next i

fullPath = fixExtension(fullPath)
fixString = fullPath
fullPath = Empty

End Function

Private Function fixExtension(ByVal exten As String)

fixExtension = Replace(exten, ".xlsx", "+")

End Function

Private Function loopFolders(ByVal patika As String)

Dim fso As Object
Dim fsoFolder As Folder

Set fso = CreateObject("Scripting.FileSystemObject")
                
        If fso.FolderExists(patika) = False Then
        MsgBox "Folder does not exist!"
        Exit Function
        End If
        
Set fsoFolder = fso.GetFolder(patika)

Dim fil As File
For Each fil In fsoFolder.Files
    
    'call function here
    If InStr(LCase(fil), ".xls") > 0 Then
    
        If counterMain < 10 Then
        tempTextMain = "00" & counterMain
        ElseIf counterMain > 9 And counterMain < 100 Then
        tempTextMain = "0" & counterMain
        Else
        tempTextMain = counterMain
        End If
    
    doStuff fil
    
    End If
    
Next fil


Dim fol As Folder
For Each fol In fsoFolder.SubFolders
    loopFolders fol.Path
Next fol

Set fso = Nothing
Set fsoFolder = Nothing

End Function


